import React from "react";
import { Link } from "react-router-dom";
import "./App.css";
function Profile() {
  return (
    <>
      <nav className="navbar">
        <div className="navbar-container">
          <Link className="navbar-logo">
            <img src="/images/logo.png" alt=""></img>
          </Link>
          <div className="searchForm">
            <input type="text" id="filter" placeholder="Search" />
            <i className="fa fa-search" aria-hidden="true"></i>
          </div>
          <Link to="/" className="browse">
            Browse
          </Link>
          <Link to="/" className="lp">
            Feed
          </Link>
          <Link to="/" className="lp">
            Events
          </Link>
          <Link to="/" className="lp">
            Profile
          </Link>
          <Link to="/" className="lp">
            Loyalty Program
          </Link>
          <img src="images/o.png" id="be" alt=""></img>
          <i className="far fa-bell" id="bell"></i>

          <Link className="btn">
            <button className="bt">Add Event</button>
          </Link>
        </div>
      </nav>
      <div id="l">
        <img src="images/leaf.png" id="le" alt="" />
        <div id="sh">
          <img src="images/dp.png" id="dp" alt="" />
          <h2>Sophie Harrison</h2>
          <i className="fas fa-calendar-check" id="tick"></i>
          <div id="sc">
            <i className="fa fa-plus" aria-hidden="true" id="plus2"></i>
          </div>
          <p>@sophie.harrison</p>
          <h6>
            Vestibulum dapibus, mauris nec malesuada fames ac turpis velit,
            rhoncus eu, luctus et interdum
            <br /> adipiscing wisi. Aliquam erat ac ipsum. Integer aliquam
            purus.
          </h6>
          <img src="images/o11.png" id="plus" alt="" />
          <i className="fa fa-plus" aria-hidden="true" id="plus1"></i>
          <h5 id="an">Add new</h5>
          <img src="images/e.png" id="e" alt="" />
          <h5 id="ch">Chill</h5>
          <img src="images/e1.png" id="e1" alt="" />
          <h5 id="ch1">Fun</h5>
          <img src="images/e2.png" id="e2" alt="" />
          <h5 id="ch2">Fun</h5>
          <img src="images/e3.png" id="e3" alt="" />
          <h5 id="ch3">Friends</h5>
        </div>
        <div id="sh1">
          <h3>Contact</h3>
          <i className="fa fa-map-marker" aria-hidden="true" id="loc"></i>
          <p>3 Bleecker St, New York, NY</p>
          <div id="line"></div>
          <i className="fa fa-phone" aria-hidden="true" id="loc1"></i>
          <p>+41 123 456 789</p>
          <div id="line"></div>
          <i className="fas fa-envelope" id="loc1"></i>
          <h5 id="ema">sophie@mutual-events.com</h5>
        </div>
        <div id="sh2">
          <table className="tbl">
            <tr>
              <th>500</th>
              <th>250</th>
              <th>100</th>
            </tr>
            <tr>
              <td>
                <i className="fa fa-users" aria-hidden="true" id="loc">
                  Followers
                </i>
              </td>
              <td>
                <i className="fas fa-user-check" aria-hidden="true" id="loc">
                  Following
                </i>
              </td>
              <td>
                <i className="fa fa-calendar" id="loc">
                  Events
                </i>
              </td>
            </tr>
          </table>
          <button className="ep">Edit profile</button>
          <button className="ep">Go live</button>
          <button id="el">My events</button>
          <button id="el1">Loyalty program</button>
        </div>
        <div className="main">
          <h4>Recap</h4>
          <h4 id="evnts">Events</h4>
          <h4>Media</h4>
          <h4>Tickets</h4>
        </div>
        <div className="i1">
          <img className="i" src="images/1.png" alt=""></img>
          <div id="mark">
            <i className="fas fa-map-marker-alt"></i>
            <p id="km"> 2.4 KM</p>
          </div>
          <img src="images/sr.png" className="sr" alt=""></img>
          <p id="pa">Sarah Rey • 04 August 2020</p>
          <h4>
            London Fashion Week Fashion <br></br> Shows & Events
          </h4>
          <img src="images/p3.png" id="p" alt="" />
          <img src="images/p2.png" id="p1" alt="" />
          <img src="images/p1.png" id="p2" alt="" />
          <h6 id="rate">4.0/5.0</h6>
          <i className="fas fa-star" id="rat"></i>
          <i className="fas fa-star" id="rat"></i>
          <i className="fas fa-star" id="rat"></i>
          <i className="fas fa-star" id="rat"></i>
          <i className="fas fa-star" id="rat1"></i>
          <div id="comm">
            <i className="far fa-comment"></i>
            <p>2 comments</p>
          </div>
          <div id="share">
            <i className="fas fa-share-alt"></i>
          </div>
        </div>
        <div className="i1">
          <img className="i" src="images/2.png" alt=""></img>
          <div id="mark">
            <i className="fas fa-map-marker-alt"></i>
            <p id="km"> 2.4 KM</p>
          </div>
          <img src="images/sr.png" className="sr" alt=""></img>
          <p id="pa">Sarah Rey • 04 August 2020</p>
          <h4>
            London Fashion Week Fashion <br></br> Shows & Events
          </h4>
          <img src="images/p3.png" id="p" alt="" />
          <img src="images/p2.png" id="p1" alt="" />
          <img src="images/p1.png" id="p2" alt="" />
          <h6 id="rate">4.0/5.0</h6>
          <i className="fas fa-star" id="rat"></i>
          <i className="fas fa-star" id="rat"></i>
          <i className="fas fa-star" id="rat"></i>
          <i className="fas fa-star" id="rat"></i>
          <i className="fas fa-star" id="rat1"></i>
          <div id="comm">
            <i className="far fa-comment"></i>
            <p>2 comments</p>
          </div>
          <div id="share">
            <i className="fas fa-share-alt"></i>
          </div>
        </div>
        <div className="i1">
          <img className="i" src="images/3.png" alt=""></img>
          <div id="mark">
            <i className="fas fa-map-marker-alt"></i>
            <p id="km"> 2.4 KM</p>
          </div>
          <img src="images/sr.png" className="sr" alt=""></img>
          <p id="pa">Sarah Rey • 04 August 2020</p>
          <h4>
            London Fashion Week Fashion <br></br> Shows & Events
          </h4>
          <img src="images/p3.png" id="p" alt="" />
          <img src="images/p2.png" id="p1" alt="" />
          <img src="images/p1.png" id="p2" alt="" />
          <h6 id="rate">4.0/5.0</h6>
          <i className="fas fa-star" id="rat"></i>
          <i className="fas fa-star" id="rat"></i>
          <i className="fas fa-star" id="rat"></i>
          <i className="fas fa-star" id="rat"></i>
          <i className="fas fa-star" id="rat1"></i>
          <div id="comm">
            <i className="far fa-comment"></i>
            <p>2 comments</p>
          </div>
          <div id="share">
            <i className="fas fa-share-alt"></i>
          </div>
        </div>
        <div className="i1">
          <img className="i" src="images/4.png" alt=""></img>
          <div id="mark">
            <i class="fas fa-map-marker-alt"></i>
            <p id="km"> 2.4 KM</p>
          </div>
          <img src="images/sr.png" className="sr" alt=""></img>
          <p id="pa">Sarah Rey • 04 August 2020</p>
          <h4>
            London Fashion Week Fashion <br></br> Shows & Events
          </h4>
          <img src="images/p3.png" id="p" alt="" />
          <img src="images/p2.png" id="p1" alt="" />
          <img src="images/p1.png" id="p2" alt="" />
          <h6 id="rate">4.0/5.0</h6>
          <i className="fas fa-star" id="rat"></i>
          <i className="fas fa-star" id="rat"></i>
          <i className="fas fa-star" id="rat"></i>
          <i className="fas fa-star" id="rat"></i>
          <i className="fas fa-star" id="rat1"></i>
          <div id="comm">
            <i className="far fa-comment"></i>
            <p>2 comments</p>
          </div>
          <div id="share">
            <i className="fas fa-share-alt"></i>
          </div>
        </div>
        <div className="i1">
          <img className="i" src="images/4.png" alt=""></img>
          <div id="mark">
            <i className="fas fa-map-marker-alt"></i>
            <p id="km"> 2.4 KM</p>
          </div>
          <img src="images/sr.png" className="sr" alt=""></img>
          <p id="pa">Sarah Rey • 04 August 2020</p>
          <h4>
            London Fashion Week Fashion <br></br> Shows & Events
          </h4>
          <img src="images/p3.png" id="p" alt="" />
          <img src="images/p2.png" id="p1" alt="" />
          <img src="images/p1.png" id="p2" alt="" />
          <h6 id="rate">4.0/5.0</h6>
          <i className="fas fa-star" id="rat"></i>
          <i className="fas fa-star" id="rat"></i>
          <i className="fas fa-star" id="rat"></i>
          <i className="fas fa-star" id="rat"></i>
          <i className="fas fa-star" id="rat1"></i>
          <div id="comm">
            <i className="far fa-comment"></i>
            <p>2 comments</p>
          </div>
          <div id="share">
            <i className="fas fa-share-alt"></i>
          </div>
        </div>
        <div className="i1">
          <img className="i" src="images/1.png" alt=""></img>
          <div id="mark">
            <i className="fas fa-map-marker-alt"></i>
            <p id="km"> 2.4 KM</p>
          </div>
          <img src="images/sr.png" className="sr" alt=""/>
          <p id="pa">Sarah Rey • 04 August 2020</p>
          <h4>
            London Fashion Week Fashion <br></br> Shows & Events
          </h4>
          <img src="images/p3.png" id="p" alt="" />
          <img src="images/p2.png" id="p1" alt="" />
          <img src="images/p1.png" id="p2" alt="" />
          <h6 id="rate">4.0/5.0</h6>
          <i className="fas fa-star" id="rat"></i>
          <i className="fas fa-star" id="rat"></i>
          <i className="fas fa-star" id="rat"></i>
          <i className="fas fa-star" id="rat"></i>
          <i className="fas fa-star" id="rat1"></i>
          <div id="comm">
            <i className="far fa-comment"></i>
            <p>2 comments</p>
          </div>
          <div id="share">
            <i className="fas fa-share-alt"></i>
          </div>
        </div>
        <div className="i1">
          <img className="i" src="images/2.png" alt=""></img>
          <div id="mark">
            <i className="fas fa-map-marker-alt"></i>
            <p id="km"> 2.4 KM</p>
          </div>
          <img src="images/sr.png" className="sr" alt=""></img>
          <p id="pa">Sarah Rey • 04 August 2020</p>
          <h4>
            London Fashion Week Fashion <br></br> Shows & Events
          </h4>
          <img src="images/p3.png" id="p" alt="" />
          <img src="images/p2.png" id="p1" alt="" />
          <img src="images/p1.png" id="p2" alt="" />
          <h6 id="rate">4.0/5.0</h6>
          <i className="fas fa-star" id="rat"></i>
          <i className="fas fa-star" id="rat"></i>
          <i className="fas fa-star" id="rat"></i>
          <i className="fas fa-star" id="rat"></i>
          <i className="fas fa-star" id="rat1"></i>
          <div id="comm">
            <i className="far fa-comment"></i>
            <p>2 comments</p>
          </div>
          <div id="share">
            <i className="fas fa-share-alt"></i>
          </div>
        </div>
      </div>
      <div className="footer1">
        <p className="s">Subscribe to receive the latest updates!</p>
        <form>
          <input type="email" className="mail" placeholder="Your email"></input>
        </form>
        <button className="su">Sign up</button>
        <div className="me">
          <img src="images/logo.png" alt=""></img>
          <p>
            Maecenas malesuada elit lectus felis, malesuada ultricies. Curabitur
            et ligula.{" "}
          </p>
          <p>Copyright © 2020 by Mutual Events.</p>
        </div>
        <div className="mu">
          <h3>Menu</h3>
          <Link to="/" id="mm">
            Browse
          </Link>
          <br></br>
          <Link to="/" id="mm">
            Feed
          </Link>
          <br></br>
          <Link to="/" id="mm">
            Events
          </Link>
          <br></br>
          <Link to="/" id="mm">
            Profile
          </Link>
        </div>
        <div className="pg">
          <h3>Programs</h3>
          <Link to="/" id="mm">
            Trending
          </Link>
          <br></br>
          <Link to="/" id="mm">
            Discover
          </Link>
        </div>
        <div className="le">
          <h3>Legal</h3>
          <p>Privacy Policy</p>
          <p>Terms of service</p>
          <p>User Guidelines</p>
        </div>
        <div id="icon">
          <i className="fab fa-facebook"></i>
          <i className="fab fa-twitter"></i>
          <i className="fab fa-linkedin"></i>
        </div>
      </div>
    </>
  );
}

export default Profile;
