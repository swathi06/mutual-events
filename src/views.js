import React from "react";
import { Link } from "react-router-dom";
import "./App.css";
function Views() {
  return (
    <div>
      <nav className="navbar">
        <div className="navbar-container">
          <Link className="navbar-logo">
            <img src="/images/logo.png" alt="logo1" />
          </Link>
          <div className="searchForm">
            <form>
              <input type="text" id="filter" placeholder="Search" />
            </form>
          </div>
          <Link to="/" className="browse">
            Browse
          </Link>
          <Link to="/page3" className="lp">
            Feed
          </Link>
          <Link to="/" className="lp">
            Events
          </Link>
          <Link to="/page4" className="lp">
            Profile
          </Link>
          <Link to="/" className="lp">
            Loyalty Program
          </Link>
          <img src="images/o.png" id="be" alt="img1" />
          <i className="far fa-bell" id="bell"></i>
          <Link className="btn">
            <button className="ae">Add Event</button>
          </Link>
        </div>
      </nav>
      <div className="main1">
        <div className="lft">
          <p>Back to event page</p>
          <h5>Reynisfjara Black Sand Beach</h5>
          <img src="images/p2.png" alt="img2" />
          <p>by Joseph Jones</p>
          <h6>Search people</h6>
          <form>
            <input type="text" placeholder="e.g.Joe Doe" id="srch" />
          </form>
          <div className="fp">
            <div className="fpr">
              <div id="jj">
                <img src="images/jj.png" alt="img3"></img>
              </div>
              <h4>Joseph Jones</h4>
              <h6>@joseph_</h6>
              <button className="FOLI">FOLLOWING</button>
            </div>
            <div className="fpr">
              <div id="jj">
                <img src="images/mc.png" alt="img4"></img>
              </div>
              <h4>Michael Scott</h4>
              <h6 id="at">@michael.scott</h6>
              <button className="FOL">FOLLOW</button>
            </div>
            <div className="fpr">
              <div id="jj">
                <img src="images/pa.png" alt="img"></img>
              </div>
              <h4>Patricia Smith</h4>
              <h6 id="at">@patricia.smith</h6>
              <button className="FOL">FOLLOW</button>
            </div>
            <div className="fpr">
              <div id="jj">
                <img src="images/ej.png" alt="img"></img>
              </div>
              <h4>Eve Johnson</h4>
              <h6 id="at">@eve.johnson</h6>
              <button className="FOL">FOLLOW</button>
            </div>
            <div className="fpr">
              <div id="jj">
                <img src="images/cg.png" alt="img"></img>
              </div>
              <h4>Camille Green</h4>
              <h6 id="at">@camille.green</h6>
              <button className="FOL">FOLLOW</button>
            </div>
            <div className="fpr">
              <div id="jj">
                <img src="images/jj.png" alt="img7"></img>
              </div>
              <h4>Joseph Jones</h4>
              <h6>@joseph_</h6>
              <button className="FOLI">FOLLOWING</button>
            </div>
            <div className="fpr">
              <div id="jj">
                <img src="images/mc.png" alt="img" />
              </div>
              <h4>Michael Scott</h4>
              <h6 id="at">@michael.scott</h6>
              <button className="FOL">FOLLOW</button>
            </div>
            <div className="fpr">
              <div id="jj">
                <img src="images/pa.png" alt="img" />
              </div>
              <h4>Patricia Smith</h4>
              <h6 id="at">@patricia.smith</h6>
              <button className="FOL">FOLLOW</button>
            </div>
            <div className="fpr">
              <div id="jj">
                <img src="images/ej.png" alt="img" />
              </div>
              <h4>Eve Johnson</h4>
              <h6 id="at">@eve.johnson</h6>
              <button className="FOL">FOLLOW</button>
            </div>
            <div className="fpr">
              <div id="jj">
                <img src="images/cg.png" alt="img" />
              </div>
              <h4>Camille Green</h4>
              <h6 id="at">@camille.green</h6>
              <button className="FOL">FOLLOW</button>
            </div>
            <div className="fpr">
              <div id="jj">
                <img src="images/jj.png" alt="jh"></img>
              </div>
              <h4>Joseph Jones</h4>
              <h6>@joseph_</h6>
              <button className="FOLI">FOLLOWING</button>
            </div>
            <div className="fpr">
              <div id="jj">
                <img src="images/mc.png" alt="img4"></img>
              </div>
              <h4>Michael Scott</h4>
              <h6 id="at">@michael.scott</h6>
              <button className="FOL">FOLLOW</button>
            </div>
            <div className="fpr">
              <div id="jj">
                <img src="images/pa.png" alt="img"></img>
              </div>
              <h4>Patricia Smith</h4>
              <h6 id="at">@patricia.smith</h6>
              <button className="FOL">FOLLOW</button>
            </div>
            <div className="fpr">
              <div id="jj">
                <img src="images/ej.png" alt="img"></img>
              </div>
              <h4>Eve Johnson</h4>
              <h6 id="at">@eve.johnson</h6>
              <button className="FOL">FOLLOW</button>
            </div>
            <div className="fpr">
              <div id="jj">
                <img src="images/cg.png" alt="img"></img>
              </div>
              <h4>Camille Green</h4>
              <h6 id="at">@camille.green</h6>
              <button className="FOL">FOLLOW</button>
            </div>
            <div className="fpr">
              <div id="jj">
                <img src="images/jj.png" alt="img"></img>
              </div>
              <h4>Joseph Jones</h4>
              <h6>@joseph_</h6>
              <button className="FOLI">FOLLOWING</button>
            </div>
            <div className="fpr">
              <div id="jj">
                <img src="images/mc.png" alt="img" />
              </div>
              <h4>Michael Scott</h4>
              <h6 id="at">@michael.scott</h6>
              <button className="FOL">FOLLOW</button>
            </div>
            <div className="fpr">
              <div id="jj">
                <img src="images/pa.png" alt="jn"></img>
              </div>
              <h4>Patricia Smith</h4>
              <h6 id="at">@patricia.smith</h6>
              <button className="FOL">FOLLOW</button>
            </div>
            <div className="fpr">
              <div id="jj">
                <img src="images/ej.png" alt="mn"></img>
              </div>
              <h4>Eve Johnson</h4>
              <h6 id="at">@eve.johnson</h6>
              <button className="FOL">FOLLOW</button>
            </div>
            <div className="fpr">
              <div id="jj">
                <img src="images/cg.png" alt="mb"></img>
              </div>
              <h4>Camille Green</h4>
              <h6 id="at">@camille.green</h6>
              <button className="FOL">FOLLOW</button>
            </div>
          </div>
        </div>
        <div className="right">
          <div id="post">
            <img src="images/o.png" alt="mk" />
            <i className="far fa-comment"></i>
            <h5>15</h5>
            <p>Posts</p>
          </div>
          <div id="post1">
            <img src="images/w.png" alt="is" />
            <i class="fa fa-eye" aria-hidden="true"></i>
            <h5>100</h5>
            <p>Views</p>
          </div>
          <Link to="/page2" id="post">
            <img src="images/o.png" alt="eeo" />
            <i className="fas fa-user-plus"></i>
            <h5>53</h5>
            <p id="att">Attending</p>
          </Link>
        </div>
      </div>

      <div className="footer">
        <p className="s">Subscribe to receive the latest updates!</p>
        <form>
          <input type="email" className="mail" placeholder="Your email"></input>
        </form>
        <button className="su">Sign up</button>
        <div className="me">
          <img src="images/logo.png" alt="logo" />
          <p>
            Maecenas malesuada elit lectus felis, malesuada ultricies. Curabitur
            et ligula.{" "}
          </p>
          <p>Copyright © 2020 by Mutual Events.</p>
        </div>
        <div className="mu">
          <h3>Menu</h3>
          <Link to="/" id="mm">
            Browse
          </Link>
          <br></br>
          <Link to="/" id="mm">
            Feed
          </Link>
          <br></br>
          <Link to="/" id="mm">
            Events
          </Link>
          <br></br>
          <Link to="/" id="mm">
            Profile
          </Link>
        </div>
        <div className="pg">
          <h3>Programs</h3>
          <Link to="/" id="mm">
            Trending
          </Link>
          <br></br>
          <Link to="/" id="mm">
            Discover
          </Link>
        </div>
        <div className="le">
          <h3>Legal</h3>
          <p>Privacy Policy</p>
          <p>Terms of service</p>
          <p>User Guidelines</p>
        </div>
        <div id="icon">
          <i className="fab fa-facebook"></i>
          <i className="fab fa-twitter"></i>
          <i className="fab fa-linkedin"></i>
        </div>
      </div>
    </div>
  );
}

export default Views;
