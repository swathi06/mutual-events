import React from "react";
import { Link } from "react-router-dom";
import "./App.js";
import "./App.css";
function App() {
  return (
    <div>
      <nav className="navbar">
        <div className="navbar-container">
          <Link className="navbar-logo">
            <img src="/images/logo.png" alt="logo"></img>
          </Link>
          <div className="searchForm">
            <form>
              <input type="text" id="filter" placeholder="Search" />
            </form>
          </div>
          <Link to="/" className="browse">
            Browse
          </Link>
          <Link to="/" className="lp">
            Feed
          </Link>
          <Link to="/" className="lp">
            Events
          </Link>
          <Link to="/" className="lp">
            Profile
          </Link>
          <Link to="/" className="lp">
            Loyalty Program
          </Link>
          <img src="images/o.png" id="be" alt="img"></img>
          <i className="far fa-bell" id="bell"></i>
          <Link className="btn">
            <button className="ae">Add Event</button>
          </Link>
        </div>
      </nav>
      <div className="main2">
        <div className="left1">
          <p>Back to event page</p>
          <h5>Reynisfjara Black Sand Beach</h5>
          <img src="images/p2.png" alt="img1" />
          <p>by Joseph Jones</p>
          <h6>Search people</h6>
          <form>
            <input type="text" placeholder="e.g.Joe Doe" id="srch" />
          </form>
          <div className="fp">
            <div className="fpr">
              <div id="jj">
                <img src="images/jj.png" alt="img2"></img>
              </div>
              <h4>Joseph Jones</h4>
              <h6>@joseph_</h6>
              <button className="FOLI">FOLLOWING</button>
            </div>
            <div className="fpr">
              <div id="jj">
                <img src="images/mc.png" alt="img4"></img>
              </div>
              <h4>Michael Scott</h4>
              <h6 id="at">@michael.scott</h6>
              <button className="FOL">FOLLOW</button>
            </div>
            <div className="fpr">
              <div id="jj">
                <img src="images/pa.png" alt="img"></img>
              </div>
              <h4>Patricia Smith</h4>
              <h6 id="at">@patricia.smith</h6>
              <button className="FOL">FOLLOW</button>
            </div>
            <div className="fpr">
              <div id="jj">
                <img src="images/ej.png" alt="img"></img>
              </div>
              <h4>Eve Johnson</h4>
              <h6 id="at">@eve.johnson</h6>
              <button className="FOL">FOLLOW</button>
            </div>
            <div className="fpr">
              <div id="jj">
                <img src="images/cg.png" alt="img"></img>
              </div>
              <h4>Camille Green</h4>
              <h6 id="at">@camille.green</h6>
              <button className="FOL">FOLLOW</button>
            </div>
            <div className="fpr">
              <div id="jj">
                <img src="images/jj.png" alt="img"></img>
              </div>
              <h4>Joseph Jones</h4>
              <h6>@joseph_</h6>
              <button className="FOLI">FOLLOWING</button>
            </div>
            <div className="fpr">
              <div id="jj">
                <img src="images/mc.png" alt="img" />
              </div>
              <h4>Michael Scott</h4>
              <h6 id="at">@michael.scott</h6>
              <button className="FOL">FOLLOW</button>
            </div>
            <div className="fpr">
              <div id="jj">
                <img src="images/pa.png" alt="img" />
              </div>
              <h4>Patricia Smith</h4>
              <h6 id="at">@patricia.smith</h6>
              <button className="FOL">FOLLOW</button>
            </div>
            <div className="fpr">
              <div id="jj">
                <img src="images/ej.png" alt="img" />
              </div>
              <h4>Eve Johnson</h4>
              <h6 id="at">@eve.johnson</h6>
              <button className="FOL">FOLLOW</button>
            </div>
            <div className="fpr">
              <div id="jj">
                <img src="images/cg.png" alt="img" />
              </div>
              <h4>Camille Green</h4>
              <h6 id="at">@camille.green</h6>
              <button className="FOL">FOLLOW</button>
            </div>
          </div>
        </div>
        <div className="right">
          <div id="post">
            <img src="images/o.png" alt="comment" />
            <i className="far fa-comment"></i>
            <h5>15</h5>
            <p>Posts</p>
          </div>
          <Link to="/" id="post">
            <img src="images/o.png" alt="eye" />
            <i className="fa fa-eye" aria-hidden="true"></i>
            <h5 id="att1">100</h5>
            <p>Views</p>
          </Link>
          <div id="post1">
            <img src="images/w.png" alt="plus" />
            <i className="fas fa-user-plus"></i>
            <h5 id="att2">53</h5>
            <p id="att">Attending</p>
          </div>
        </div>
      </div>

      <div className="footer">
        <p className="s">Subscribe to receive the latest updates!</p>
        <form>
          <input type="email" className="mail" placeholder="Your email"></input>
        </form>
        <button className="su">Sign up</button>
        <div className="me">
          <img src="images/logo.png" alt="logo" />
          <p>
            Maecenas malesuada elit lectus felis, malesuada ultricies. Curabitur
            et ligula.{" "}
          </p>
          <p>Copyright © 2020 by Mutual Events.</p>
        </div>
        <div className="mu">
          <h3>Menu</h3>
          <Link to="/" id="mm">
            Browse
          </Link>
          <br></br>
          <Link to="/" id="mm">
            Feed
          </Link>
          <br></br>
          <Link to="/" id="mm">
            Events
          </Link>
          <br></br>
          <Link to="/" id="mm">
            Profile
          </Link>
        </div>
        <div className="pg">
          <h3>Programs</h3>
          <Link to="/" id="mm">
            Trending
          </Link>
          <br></br>
          <Link to="/" id="mm">
            Discover
          </Link>
        </div>
        <div className="le">
          <h3>Legal</h3>
          <p>Privacy Policy</p>
          <p>Terms of service</p>
          <p>User Guidelines</p>
        </div>
        <div id="icon">
          <i className="fab fa-facebook"></i>
          <i className="fab fa-twitter"></i>
          <i className="fab fa-linkedin"></i>
        </div>
      </div>
    </div>
  );
}

export default App;
