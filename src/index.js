import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter, Route, Switch } from "react-router-dom";
import Views from "./views";
import Feed from "./feed";
import Attending from "./attending";
import App from './App'; 
import Profile from './profile';
ReactDOM.render(
  <App />,
    document.getElementById('root'));
     const rootElement = document.getElementById("root");
     ReactDOM.render(
       <BrowserRouter>
        <Switch>
         <Route exact path="/" component={Views} />
         <Route path="/page2" component={Attending} />
         <Route path="/page3" component={Feed} />
         <Route path="/page4" component={Profile} />
       </Switch>
       </BrowserRouter>,
       rootElement
     );

