import React from "react";
import { Link } from "react-router-dom";
import "./App.css";
function Navbar() {
    return (
      <div>
        <nav className="navbar">
          <div className="navbar-container">
            <Link className="navbar-logo">
              <img src="/images/logo.png" alt="logo1" />
            </Link>
            <div className="searchForm">
              <form>
                <input type="text" id="filter" placeholder="Search" />
              </form>
            </div>
            <Link to="/" className="browse">
              Browse
            </Link>
            <Link to="/" className="lp">
              Feed
            </Link>
            <Link to="/" className="lp">
              Events
            </Link>
            <Link to="/" className="lp">
              Profile
            </Link>
            <Link to="/" className="lp">
              Loyalty Program
            </Link>
            <img src="images/o.png" id="be" alt="img1" />
            <i className="far fa-bell" id="bell"></i>
            <Link className="btn">
              <button className="ae">Add Event</button>
            </Link>
          </div>
        </nav>
        </div>
    )}
export default Navbar;