import React ,{useState} from 'react';
import "./App.css";

function Feed() {
    const [show,setShow]=useState(false)
    return (
        <div>
           <div className="list" >
              <div id="main">
              <img src="images/ej.png" id="ej" alt="dp"/>
              <h5>Eve Johnson</h5>
              <p id="lp">10 min ago</p>
              <div className="rht">
              <button className="dot">...</button>
              <div className="dropdown">
                  <a href="#item">Item 1</a>
                  <a href="#item">Item 2</a>
                  <a href="#item">Item 3</a>
              </div>
              </div>
              <div id="images">
              <div id="dis">
              <i className="fas fa-map-marker-alt"></i>     
              <h6>2.4 KM</h6>
              </div>
              <img src="images/ej1.png" id="img" alt="image1"/>
              <img src="images/ej2.png" id="img" alt="image2"/>
              <img src="images/ej3.png" id="img" alt="image3"/>
              <img src="images/ej4.png" id="img" alt="image4"/>
              <img src="images/ej5.png" id="img" alt="image5"/>
              <h3>+3</h3>
              </div>
              <div id="comt">
              <img src="images/Ir.png" alt="ireland"/>
              <h4>What’s new in Ireland</h4>
              <p>Event starts: Aug 21, 2020</p>
              <div id="di"></div>
              </div>
              </div>
              <div id="com">
              <i className="fas fa-heart"></i>
              <p>15</p>
              </div>
              <div id="co">
              <i className="far fa-comment"></i>
                 <p >2 comments</p>
                 </div>
                 <div id="view">
                 <i className="fa fa-eye" aria-hidden="true"></i>
                 <p>3</p>
                 </div>
                 <div id="share">
                 <i className="fas fa-share-alt" ></i>
                 <p>Share</p>
                 </div>
                 <div id="sr">
                 <img  src="images/p3.png" alt="sarah"/>
                 <p>sarah.connor</p>
                 <h6>This is a sample comment</h6>
                 <button onClick={()=>setShow(true)}>View answer</button>
                 {
                         show?<h5>This is a Sample reply</h5>:null
                 }
                 </div>
                 <div id="as">
                 <img  src="images/p1.png" alt="sarah"/>
                 <p>alice_scott</p>
                 <h6>This is a sample comment</h6>            
                 </div>
                 <div id="comment">
                   <img src="images/cmt.png" alt="dp"/>
                   <input type="text" placeholder="Add comment..." id="add"></input>               
                   <button id="send">
                   <i className="far fa-paper-plane"></i>
                       SEND</button>
                 </div>        
           </div>
        </div>
    )
}

export default Feed;
